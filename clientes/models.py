from django.db import models

# Create your models here.

class Client(models.Model):
    ALTA = '1'
    MEDIA = '2'
    BAJA = '3'
   
    PRIORIDAD_CLIENTE = (
        (ALTA, 'Alta'),
        (MEDIA, 'Normal'),
        (BAJA, 'Baja'),
    )
    
    identidad = models.IntegerField(help_text="Cédula")
    ticket = models.CharField(max_length=5, help_text="Número de ticket")    
    prioridad = models.CharField(
        max_length=20,
        choices=PRIORIDAD_CLIENTE,
        default='2',
        blank=True,
        help_text="Prioridad del cliente"
    )
    
    def __str__(self):
        return self.ticket