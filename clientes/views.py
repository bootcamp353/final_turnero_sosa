from django.shortcuts import render
from .models import Client
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
import random



# Create your views here.
def index(request):
    db_data_alta = Client.objects.filter(prioridad='Alta')
    db_data_normal = Client.objects.filter(prioridad='Normal')
    db_data_baja = Client.objects.filter(prioridad='Baja')
    if  db_data_alta :
        db_data = db_data_alta
    if not db_data_alta :
        db_data = db_data_normal
    if(not db_data_alta and not db_data_normal ):
        db_data = db_data_baja
    context = {
        "db_data_alta": db_data_alta,
        "db_data_normal": db_data_normal,
        "db_data_baja": db_data_baja,
        "db_data": db_data,
        "update": None
    }
    return render(request, "client/index.html", context)

def insert(request):     
    caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' 
    codigoresultante = "".join(random.choice(caracteres) for i in range(6))  
    try:
        identidad = request.POST["identidad"]
        prioridad = request.POST["prioridad"]
        if identidad == "" or prioridad == "":
            raise ValueError("El texto no puede estar en vacio.")
        db_data = Client(identidad=identidad, ticket=codigoresultante, prioridad=prioridad)
        db_data.save()
        return HttpResponseRedirect(reverse("index")) 
    except ValueError as err:
        print(err)
        return HttpResponseRedirect(reverse("index")) 


def update(request):
    client_id = request.POST["id"]
    identidad = request.POST["identidad"]
    prioridad = request.POST["prioridad"]
    db_data = Client.objects.get(pk=client_id)
    db_data.identidad = identidad
    db_data.prioridad = prioridad
    db_data.save()
    return HttpResponseRedirect(reverse("index")) 


def update_form(request, client_id):
    db_data = Client.objects.all()
    db_data_only = Client.objects.get(pk=client_id)
    print(db_data_only)
    context = {
        "db_data": db_data[::-1],
        "update": db_data_only
    }
    return render(request, "client/index.html", context)


def delete(request, client_id):
    db_data = Client.objects.filter(id=client_id)
    db_data.delete()
    return HttpResponseRedirect(reverse("index")) 