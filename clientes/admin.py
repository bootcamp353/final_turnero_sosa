from django.contrib import admin
from . import models
# Register your models here.

@admin.register(models.Client)
class Clientes(admin.ModelAdmin):
    list_display = ('ticket', 'identidad', 'prioridad',)
    search_fields = ('identidad','ticket',)
